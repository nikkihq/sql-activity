CREATE DATABASE timetracker;

-- 1 (Creating the table department with id and name attributes)
CREATE TABLE departments(
    id BIGINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255),
    PRIMARY KEY (id)
);

-- 2 (Altering table departments to alter the id as the primary key)
ALTER TABLE departments ADD CONSTRAINT PRIMARY KEY (id);

-- 3 (Creating table statuses with id and name attributes)
CREATE TABLE statuses(
    id BIGINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255),
    PRIMARY KEY (id)
);

-- 4 (Altering the table statuses to alter the id as the primary key )
ALTER TABLE statuses ADD CONSTRAINT PRIMARY KEY (id);

-- 5 (Creating table timeTypes with id, name, type Varchar as an attribute)
CREATE TABLE timeTypes(
    id BIGINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255),
    type VARCHAR(255),
    PRIMARY KEY (id)
);

-- 6 (Altering table timeTypes to set the id as the primary key)
ALTER TABLE timeTypes ADD CONSTRAINT PRIMARY KEY (id);

-- 7 (Altering table timeTypes to modify the datatype from VARCHAR to Int)
ALTER TABLE timeTypes MODIFY type INT;

-- 8 (Dropping the column "type" from table timeTypes)
ALTER TABLE timeTypes DROP COLUMN type;

-- 9 (Creating the table timeIns wit id, date, time, typeId (as foreign key), statusId (as FOREIGN KEY))
CREATE TABLE timeIns(
    id BIGINT NOT NULL AUTO_INCREMENT,
    time time,
    date date,
    timeTypeId BIGINT NOT NULL,
    statusId BIGINT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (timeTypeId) REFERENCES timeTypes (id),
    FOREIGN KEY (statusId) REFERENCES statuses (id)
);

-- 10 (Creating table employees with id, firtName, lastName, employeeId, designation and departmentId set as foreign key)
CREATE TABLE employees(
    id BIGINT NOT NULL AUTO_INCREMENT,
    firstName VARCHAR(255),
    lastName VARCHAR(255),
    designation VARCHAR(255),
    employeeId VARCHAR(255),
    departmentId BIGINT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (departmentId) REFERENCES departments(id)
);

--INSERT INTO (tableNames)(
--    columns..
--)Values(
--    values
--)

INSERT INTO timeTypes(
    id, name
    )VALUES(
    1, "Time in");

INSERT INTO timeTypes(
    id,
    name
    )VALUES(
    2,
    "Time out");

INSERT INTO departments(
    id,
    name
    )VALUES(
    1,
    "IT");

INSERT INTO departments(
    id,
    name
    )VALUES(
    2,
    "ACCOUNTING");

INSERT INTO departments(
    id,
    name
    )VALUES(
    3,
    "HR");

INSERT INTO departments(
    id,
    name
    )VALUES(
    4,
    "SALES");

INSERT INTO timeIns (date, time, timeTypeId, statusId, employeeId) VALUES ('2021-4-1', '2:01 AM', 1, 1, 1);
INSERT INTO timeIns (date, time, timeTypeId, statusId, employeeId) VALUES ('2019-5-8', '2:02 AM', 1, 1, 2);
INSERT INTO timeIns (date, time, timeTypeId, statusId, employeeId) VALUES ('2021-11-9', '2:03 AM', 1, 1, 3);
INSERT INTO timeIns (date, time, timeTypeId, statusId, employeeId) VALUES ('2020-12-12', '2:04 AM', 1, 1, 4);
INSERT INTO timeIns (date, time, timeTypeId, statusId, employeeId) VALUES ('2021-4-1', '2:01 AM', 1, 1, 5);


INSERT INTO statuses (id, name) VALUES (1, "LATE");
INSERT INTO statuses (id, name) VALUES (2, "ON-TIME");
INSERT INTO statuses (id, name) VALUES (3, "EARLY");

ALTER TABLE timeIns ADD COLUMN employeeId BIGINT REFERENCES employees(id);


--Update a record/s
--UPDATE (tableNames)
--SET columnName = value,columnName = value
--WHERE employeeId= 1

UPDATE timeIns SET time = "07:00:00" WHERE employeeId = 1;

--Delete records from table
DELETE FROM timeIns WHERE employeeId = 5;

--Activity 2
--1. Delete all "Automation Specialist III"
--2. Update all Marketing department employee's designation to "Marketing Specialist II""
--3. Update all IT department employees designation to "Software Engineer III"
--4. Update all "Software Engineer II"'s department to "IT"
DELETE FROM employees WHERE designation = "Automation Specialist III";
UPDATE employees SET designation = "Marketing Specialist II" WHERE departmentId = 5;
UPDATE employees SET designation = "Software Engineer III" WHERE departmentId = 1;
UPDATE employees SET departmentId = 1 WHERE designation = "Software Engineer II";



CREATE TABLE employees (
  id BIGINT NOT NULL AUTO_INCREMENT,
  firstName VARCHAR(50),
	lastName VARCHAR(50),
	employeeId VARCHAR(50),
	gender VARCHAR(50),
	designation VARCHAR(50),
	departmentId BIGINT NOT NULL,
	age INT,
	hiringYear INT,
	streetAddress VARCHAR(50),
	city VARCHAR(50),
	country VARCHAR(50),
  PRIMARY KEY (id),
  FOREIGN KEY (departmentId) REFERENCES departments(id)
);

